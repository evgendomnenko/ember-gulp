import Ember from "ember";
import Resolver from "ember/resolver";

function chooseModuleName(moduleEntries, moduleName) {
    var underscoredModuleName = Ember.String.underscore(moduleName);

    if (moduleName !== underscoredModuleName && moduleEntries[moduleName] && moduleEntries[underscoredModuleName]) {
      throw new TypeError("Ambiguous module names: `" + moduleName + "` and `" + underscoredModuleName + "`");
    }

    if (moduleEntries[moduleName]) {
      return moduleName;
    } else if (moduleEntries[underscoredModuleName]) {
      return underscoredModuleName;
    } else {
      // workaround for dasherized partials:
      // something/something/-something => something/something/_something
      var partializedModuleName = moduleName.replace(/\/-([^\/]*)$/, '/_$1');

      if (moduleEntries[partializedModuleName]) {
        Ember.deprecate('Modules should not contain underscores. ' +
                        'Attempted to lookup "'+moduleName+'" which ' +
                        'was not found. Please rename "'+partializedModuleName+'" '+
                        'to "'+moduleName+'" instead.', false);

        return partializedModuleName;
      } else {
        return moduleName;
      }
    }
  }

var BrandResolver = Resolver.extend({
	brandFallbackChain: Ember.computed(function() {
		return Ember.A(this.namespace.brandFallbackChain || ['default']);
	}),

	brandEnabledPodBasedModuleName: function(brand, parsedName) {
      var prefix = this.namespace.modulePrefix;
	  var fullNameWithoutType = parsedName.fullNameWithoutType;

      return prefix + '/brands/' + brand + '/pods/' + fullNameWithoutType + '/' + parsedName.type;
    },

	barndEnavledModuleName: function(brand, parsedName) {
		var fullNameWithoutType = parsedName.fullNameWithoutType;
		var prefix = this.namespace.modulePrefix;

		if(parsedName.type === 'template') {
			return prefix + '/' + this.pluralize(parsedName.type) + '/brands/' + brand + '/' + parsedName.fullNameWithoutType;
		}

		return prefix + '/brands/' + brand + '/' + this.pluralize(parsedName.type) + '/' + parsedName.fullNameWithoutType;
	},

	 /**

      A listing of functions to test for brand enabled moduleName's based on the provided
      `parsedName`. This allows easy customization of additional module based
      lookup patterns.

      @property brandModuleNameLookupPatterns
      @returns {Ember.Array}
    */
    brandModuleNameLookupPatterns: Ember.computed(function() {
      return Ember.A([
        this.brandEnabledPodBasedModuleName,
		this.barndEnavledModuleName
      ]);
    }),

	findModuleName: function(parsedName, loggingDisabled) {
      var self = this;
      var moduleName;

      this.get('brandModuleNameLookupPatterns').find(function(pattern) {
        var moduleEntries = requirejs.entries;
		self.get('brandFallbackChain').find(function(brand) {
			var tmpModuleName = pattern.call(self, brand, parsedName);

			// allow treat all dashed and all underscored as the same thing
			// supports components with dashes and other stuff with underscores.
			if (tmpModuleName) {
			  tmpModuleName = chooseModuleName(moduleEntries, tmpModuleName);
			}

			if (tmpModuleName && moduleEntries[tmpModuleName]) {
			  if (!loggingDisabled) {
				self._logLookup(true, parsedName, tmpModuleName);
			  }

			  moduleName = tmpModuleName;
			}

			if (!loggingDisabled) {
			  self._logLookup(moduleName, parsedName, tmpModuleName);
			}

			return moduleName;
		});

        return moduleName;
      });

      return moduleName || this._super(parsedName, loggingDisabled);
    }
});

BrandResolver['default'] = BrandResolver;
export default BrandResolver;

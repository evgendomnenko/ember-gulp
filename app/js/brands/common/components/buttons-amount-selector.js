import Ember from 'ember';

var DEFAULT_AMOUNTS = [25, 50, 100, 200];

export default Ember.Component.extend({
    layoutName: 'components/buttons-amount-selector',
    
    predefinedAmounts: DEFAULT_AMOUNTS,
    
    actions: {
        setAmount: function(amount) {
            this.set('amount', amount);
        }
    }
});

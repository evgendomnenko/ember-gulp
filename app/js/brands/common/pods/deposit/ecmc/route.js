import DepositMethodRoute from "../../deposit/credit-card/route";

export default DepositMethodRoute.extend({ 
    setupController: function(controller, model) {
        this._super(controller, model);
        controller.set('title', 'Ecmc Deposit');
    }
});


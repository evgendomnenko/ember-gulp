import amountSelector from "../../../components/buttons-amount-selector";
export default {
	name: 'all-brands-amount-selector',
	initialize: function(container, application) {
		application.register('component:amount-selector', amountSelector); 
	}
};

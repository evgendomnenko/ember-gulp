import amountSelector from "../../../components/slider-amount-selector";
export default {
	name: 'bets10-amount-selector',
	initialize: function(container, application) {
		application.register('component:amount-selector', amountSelector); 
	}
};

import Ember from 'ember';
import Resolver from './brand-resolver';
import loadInitializers from 'ember/load-initializers';
//import config from './config/environment';
import brandConfig from './brands-config';

Ember.MODEL_FACTORY_INJECTIONS = true;

var App = Ember.Application.extend({
    modulePrefix: 'pp5',
    podModulePrefix: 'pp5',
	  brandFallbackChain: brandConfig,
    LOG_VIEW_LOOKUPS: true,
    LOG_RESOLVER: true,
    Resolver: Resolver
});

loadInitializers(App, '');
brandConfig.reverse().forEach(function(brand) {
	loadInitializers(App, '/brands/' + brand);
});
// Reverse array again to return it to the order
brandConfig.reverse();

App.create();

export default App;

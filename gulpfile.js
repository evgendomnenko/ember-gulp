/**
 * Created by evgendomnenko on 17.12.14.
 */

var gulp = require('gulp');
var bower = require('main-bower-files');
var concat = require('gulp-concat');
var less = require('gulp-less');
var argv = require('yargs').argv;
var merge = require('gulp-merge');
var transpiler = require('gulp-es6-module-transpiler');
var emberTemplates = require('gulp-ember-templates');
var fs = require('fs');
var del =  require('del');
var rename = require('gulp-rename');
var file = require('gulp-file');

var brand = argv.brand ? argv.brand.toLowerCase() : "bets10";
var brandsConfig = JSON.parse(fs.readFileSync('./app/config/brands-config.json', 'utf8'));
var brandConfig = brandsConfig[brand] || [ brand, "common" ];

gulp.task('vendor', function () {
  return gulp.src(bower())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('js', function () {
  var brandPath,
    brandJsPathChain = [],
    barndHbsPathChain = [],
    brandChain = brandConfig.slice().reverse();

  while(brandPath = brandChain.pop()) {
    brandPath =  brandPath.toLowerCase();
    brandJsPathChain.push('./app/js/brands/' + brandPath + '/**/*.js');
    barndHbsPathChain.push('./app/js/brands/' + brandPath + '/**/*.hbs')
  }
  brandJsPathChain.push('./app/js/*.js');

  return merge(
    gulp.src('./app/js/**/*.hbs')
      .pipe(rename({ extname: '.js' }))
      .pipe(emberTemplates({ type: 'es6' })),
    gulp.src(brandJsPathChain, { base: './app/js' })
  )
    .pipe(file(
      'brands-config.js',
      'export default ' + JSON.stringify(brandConfig) + ';'))
    .pipe(rename(function (path) {
      path.dirname = 'pp5/' + path.dirname;
    }))
    .pipe(transpiler({ type: 'amd' }))
    .pipe(concat('./brand.js'))
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('html', function () {
  return gulp.src('./app/index.html')
    .pipe(gulp.dest('./dist'))
});

gulp.task('less', function () {
  return gulp.src('./app/styles/brands/' + brand + '/brand.less')
    .pipe(less())
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('clean', function () {
  return del('dist');
});

gulp.task('build', ['clean', 'vendor', 'js', 'less', 'html']);


